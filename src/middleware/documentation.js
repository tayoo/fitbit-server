const express = require("express")
const swaggerUi = require("swagger-ui-express")
const yamlJs = require("yamljs")
const fs = require("fs")

const swaggerDocument = yamlJs.parse(fs.readFileSync(__dirname + "/../api/api.yaml", "utf8"))

const documentation = express
	.Router()
	.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument))
	.get("/swagger.json", (req, res) => {
		res.send(swaggerDocument)
	})
	.get("/favicon.ico", (req, res) => {
		res.writeHead(200, {'Content-Type': 'image/x-icon'} );
		res.end();
		console.log('favicon requested');
		return;
	})

module.exports = documentation
