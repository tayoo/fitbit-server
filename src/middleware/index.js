const requestLogger = require("./requestLogger")
const documentation = require("./documentation")
const helmet = require("./helmet")
const cors = require("./cors")
const bodyParser = require("express").json()
const liveness = require("./liveness")
const validate = require("./validator")

module.exports = {
	bodyParser,
	cors,
	helmet,
	requestLogger,
	documentation,
	liveness,
	validate,
}
