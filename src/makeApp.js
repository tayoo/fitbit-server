const express = require("express")
const SwaggerParser = require("swagger-parser")
const {connector} = require("swagger-routes-express")
const api = require("./api")
const loadMiddleware = require("./utils/loadMiddleware")
const middleware = require("./middleware")
const OpenApiValidator = require("express-openapi-validator")
const errorHandler = require("./utils/errorHandler")
const makeApp = async () => {
	const parser = new SwaggerParser()
	const apiDefinitionLocation = "./src/api/api.yaml"
	const apiDescription = await parser.validate(apiDefinitionLocation)
	const connect = connector(api, apiDescription)
	const app = express()
	loadMiddleware(app, middleware)
	connect(app)
	errorHandler(app)
	app.use(
		OpenApiValidator.middleware({
			apiSpec: apiDefinitionLocation,
			validateResponses: false,
			validateRequests: true,
		})
	)
	return app
}

module.exports = makeApp
