const loadMiddleware = (app, middleware) => {
	for (const middle of Object.keys(middleware)) {
		app.use(middleware[middle])
	}
}

module.exports = loadMiddleware
