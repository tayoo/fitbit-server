const validateStringField = (field) => {
	if (!field || field.trim().length <= 0) {
		return false
	}
	return true
}

module.exports = {validateStringField}
