const { DynamoDBClient, PutItemCommand } = require("@aws-sdk/client-dynamodb"); // CommonJS import

const client = new DynamoDBClient({ 
    region: "us-east-1" ,
    credentials: {
        accessKeyId: 'AKIATWAL3G4VB5IMV37T',
        secretAccessKey: '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'
      }
});

const postFitbitData = async (req, res) => {
    const sensorName = req.body.sensorName
    const timestamp = req.body.timestamp
    let x = '',y= '',z = '', value=''
    if (req.body.data.x){
        x = req.body.data.x
    }
    if (req.body.data.y){
        y = req.body.data.y
    }
    if (req.body.data.z){
        z = req.body.data.z
    }
    if (req.body.data.value){
        value = req.body.data.value
    }
   console.log('sensorName : '+sensorName)
   console.log('timestamp : '+timestamp)
   console.log('data : '+{x,y,z, value})
    var params = {
        TableName: 'FITBIT_DATA',
        Item: {
            'sensor_name' : {S: sensorName},
            'timestamp' : {N: `${timestamp}`},
            'x_value' : {S:`${ x}`},
            'y_value' : {S: `${y}`},
            'z_value' : {S: `${z}`},
            'value': {S:`${ value}`} 
        }
      };
    const command = new PutItemCommand(params);
    // async/await.
    await client.send(command).then(
    (data) => {
        console.log('data: '+JSON.stringify(data))
    },
    (error) => {
        console.log('error 1:  '+error)
        console.log('error: '+JSON.stringify(error))
    }
    );
	res.status(200).send("success")
}

module.exports = postFitbitData
