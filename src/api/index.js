const postFitbitData = require("./postFitbitData")
const getHealth = require("./v1/healthcheck/getHealth")
module.exports = {
	postFitbitData,
	getHealth
}
