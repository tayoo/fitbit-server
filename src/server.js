const makeApp = require("./makeApp")

makeApp()
	.then((app) => app.listen(3000))
	.then(() => console.log("Server started " + 3000))
	.catch((err) => console.error("Fatal server error", err))
